package com.Bsf.Email.HomePageTest;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Bsf.Email.BaseClassOf_Tests.TestBase;
import com.Bsf.Email.PageFactory.HomePage;
import com.Bsf.Email.PageFactory.LoginPage;

public class TC02_VerifyHomePage extends TestBase {
	LoginPage loginpag;
	HomePage homepage;
	
	public static final Logger log = Logger.getLogger(TC02_VerifyHomePage.class.getName());
	
	@BeforeTest
    public void SetUp() {
		
	    init();
	    
	  }
	 @Test(priority=3)
	  public void VerifyLoginWith_Valid_User_Pass() 
	 {
		  log.info("====== Starting test =====");
		  loginpag = new LoginPage(driver);
		  
		  
		  loginpag.setusername("anees");
		  loginpag.setpassword("password1234");
		  loginpag.click_button();
		 
		 
		 //Assert.assertEquals(driver.findElement(By.xpath("//div[@id='email-form']/div/h1")).getText(),"BSF Bulk Email Tool" );
		 log.info("====== Ending test ======");
		  
	  }
	  @Test(priority=4)
	  public void VerifyHomePage_ValidData()
	  {
		  
		  homepage = new HomePage(driver);
		  homepage.Enter_fromname("Veer Singh");
		  homepage.SelectEmailList();
		  
		  homepage.Select_CheckBox();
		  homepage.Enteredsubject("BSf international");
		  homepage.EntredText_inMsgBox("Hi team bsjad dfksgfhks gdfhsgfdsku dfs");
		  homepage.EnterTestEmailADD("rajveer.it87@gmail.com");
		  homepage.SendButtons();
		  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  }
}
