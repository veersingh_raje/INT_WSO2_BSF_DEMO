package com.Bsf.Email.HomePageTest;

import org.testng.annotations.Test;

import com.Bsf.Email.BaseClassOf_Tests.TestBase;
import com.Bsf.Email.PageFactory.HomePage;
import com.Bsf.Email.PageFactory.LoginPage;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;

public class TC01_VerifyLoginPage extends TestBase {
	public static final Logger log = Logger.getLogger(TC01_VerifyLoginPage.class.getName());
	
	LoginPage loginpag;
	
	HomePage homepage;
	@BeforeTest
    public void SetUp() {
		
	    init();
	    
	  }
	
	
	@Test(priority=1)
	  public void VerifyLoginWith_Blank_User_Pass() {
		  
		  log.info("====== Starting test =====");
		  loginpag = new LoginPage(driver);
		  
		  
		  loginpag.setusername("");
		  loginpag.setpassword("");
		  loginpag.click_button();
		  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		  
		  String emptyUserNamePasswordMessage =  "Username or password cannot be empty";
		
	      String errorMessage = driver.findElement(By.xpath("//div[1]/p[@id='error']")).getText();
	      Assert.assertEquals(emptyUserNamePasswordMessage,errorMessage);
		
		  log.info("====== Ending test ======");
		
	  }
	  
	
  @Test(priority=2)
  public void VerifyLoginWith_InValid_User_Pass() {
	  log.info("====== Starting test =====");
	  loginpag = new LoginPage(driver);
	  
	  
	  loginpag.setusername("ane");
	  loginpag.setpassword("passd12");
	  loginpag.click_button();
	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  /*
	   error message is not empty
	   error message is "invalid credentials"
	   */
	
	 
	 String expectedErrorMessage = "invalid credentials";
	 String actualErrorMessage = driver.findElement(By.xpath("//p[@id='error']")).getText();
	
	 Assert.assertEquals(expectedErrorMessage, actualErrorMessage.concat("invalid credentials"));
	 
	 log.info("====== Ending test ======");
	
	  
  }
  
 
  @AfterClass
  public void endTest() {
	  
	  //driver.close();
  }

  

}
