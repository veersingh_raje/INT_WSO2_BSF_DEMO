package com.Bsf.Email.PageFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
	
	public static final Logger log = Logger.getLogger(HomePage.class.getName());
	
	WebDriver driver;
	
	@FindBy(xpath="//input[@id='fromname']")
	WebElement FromName;
	 
	
	@FindBy(id="fromemail")
	WebElement Select_dropdown;
	
	@FindBy(xpath="//input[@id='selectedleaders']")
	WebElement selectedLeader;
	
	@FindBy(xpath="//input[@id='sendtoall']")
	WebElement SelectAllcheck;
	
	@FindBy(xpath="//input[@id='ACA']")
	WebElement ACA_Grpoucheck;
	
	@FindBy(name="region")
	WebElement UGL_ResionCheck;
	
	@FindBy(name="schedule")
	WebElement classSchedule_check;
	
	@FindBy(name="type")
	WebElement classtype_Check;
	
	@FindBy(xpath="//input[@name='subject']")
	WebElement Entersubject;
	
	@FindBy(xpath="//input[@id='testto']")
	WebElement TestEmailAddress;
	
	@FindBy(css="::before")
	WebElement msgTextbox1;
	
	@FindBy(xpath="//ul[@id='emailul']/li[6]/div[2]/div[3]/div[4]/p[3]")
	WebElement msgTextbox3;
	
	/*@FindBy(xpath="//div[3]/div[4]/p[5]")
	WebElement msgTextbox5;
	
	@FindBy(xpath="//div[3]/div[4]/p[6]")
	WebElement msgTextbox6;*/
	
	@FindBy(id="testmail")
	WebElement clickTestmailButton;
	
	@FindBy(id="sendmail")
	WebElement clickSendMailButton;
	
	@FindBy(id="cancel")
	WebElement clickCancelButton;
	
	public HomePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public void Enter_fromname(String fromname)
	{
		FromName.sendKeys(fromname);
		
		
	} 
	
	public void SelectEmailList()
	{
		
		
		Select Email=new Select(Select_dropdown);
		Email.selectByVisibleText("BSF_Survey@bsfinternational.org");
	}
	
	public void Select_CheckBox()
	{
		        //SelectAllcheck.click();
				selectedLeader.click();
				ACA_Grpoucheck.click();
				UGL_ResionCheck.click();
				classSchedule_check.click();
				classtype_Check.click();
	}
	
	
	public void Enteredsubject(String subject)
	{
		Entersubject.sendKeys(subject);
		
	}
	
	
	public void EntredText_inMsgBox(String messag)
	{
		//msgTextbox1.sendKeys(input1);
		//msgTextbox3.sendKeys(input2);
		Entersubject.sendKeys(Keys.TAB+messag);
	
		
	}
	
	public void EnterTestEmailADD(String testEmailadd)
	{
		TestEmailAddress.sendKeys(testEmailadd);
	}
	
	
	public void SendButtons()
	{
		//clickTestmailButton.click();
		clickSendMailButton.click();
		//clickCancelButton.click();
		
	}
	

}
