package com.Bsf.Email.PageFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	public static final Logger log = Logger.getLogger(LoginPage.class.getName());
	WebDriver driver;
	
	@FindBy(id="username")
	WebElement UserName;
	 
	@FindBy(id="password")
	WebElement PassWord;
	
	@FindBy(name="Submit")
	WebElement SubmitButton;
	
	public LoginPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public void setusername(String username)
	{
		UserName.clear();
		UserName.sendKeys(username);
		log.info("Entered Username "+username);
		
	}
	
	public void setpassword(String password)
	{   PassWord.clear();
		PassWord.sendKeys(password);
		log.info("Entered password "+password);
	}
	
	public void click_button()
	{
		SubmitButton.click();
		log.info("Clicked on Submit Button ");
	}
}
