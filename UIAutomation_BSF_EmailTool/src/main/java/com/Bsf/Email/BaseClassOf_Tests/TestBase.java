package com.Bsf.Email.BaseClassOf_Tests;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestBase {

	public static final Logger log = Logger.getLogger(TestBase.class.getName());
	
	public WebDriver driver;
	String url = "http://frontend-mail.s3-website-us-east-1.amazonaws.com/login.html";
	String Browser = "chrome";
	//String Browser = "firefox";
	//String Browser = "HtmlUintDriver";
	
	public void init(){
		SelectBrowser(Browser);
		getUrl(url);
		
		String log4jConfPath = "Log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);
		
	}
	
	public void SelectBrowser(String Browser)
	{
		if(Browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Drivers\\chromedriver.exe");
			log.info("Creating object of "+Browser);
			driver = new ChromeDriver();
		}
		if(Browser.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Drivers\\geckodriver.exe");
			log.info("Creating object of "+Browser);
			driver = new FirefoxDriver();
		}
		
		if(Browser.equalsIgnoreCase("HtmlUintDriver"))
		{
			driver= new HtmlUnitDriver();
			log.info("Creating object of "+Browser);
		}
	}
	
	public void getUrl(String url)
	{
		log.info("Navigating to url "+url);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
}
